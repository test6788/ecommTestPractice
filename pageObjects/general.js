//This example is going to test: check the status code for each page of the header menu and open it.
//check logo

module.exports = function (browser){
    this.openBrowser = function() {
        browser
            .url('https://riveravargasmiguel.wixsite.com/mollystorm')
            .resizeWindow(1400, 3000)
        return browser
    };//end openBrowser

    this.page1 = function(){
        browser
        .verify.ok(true, "'https://riveravargasmiguel.wixsite.com/mollystorm' opened sucessfully")
        .useXpath()
        .waitForElementVisible('/html/body', 10000)
        .pause(5000)
        // Take one screenshot 
        .saveScreenshot('pathForScreenshots/Production/Screenshots/home_Page.png')
        .pause(3000)
        const request = require('request');
        request('https://riveravargasmiguel.wixsite.com/mollystorm', function (error, response, body) {
                let responseURL = response.statusCode;
                console.log("RESPONSE STATUS: " + responseURL)
                if (request === 404){
                    browser
                    .verify.equal(response.statusCode, 200)
                    .verify.ok(false,"URL does not work")
                }
                else {
                    browser
                    .verify.ok(true,"Url is good")
                }
            })
    }
//general action to use to interact with the page



    this.switching = function(){
        browser
        //Switch back to the first window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[0];
            this.switchWindow(newWindow);
        })
    }

    this.switchingWin2 = function(){
        browser
        //Switch back to the second window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[1];
            this.switchWindow(newWindow);
        })
    }


    this.back = function() {
        browser
        .execute(function () {
            window.history.back()
        })//to go to the previous page
        .pause(4000)
    }

    this.previews = function(){
        browser
        .back()
        consentMessage()
        browser
        .useXpath()
    }

    //important parts of the home page

    this.headMenu = function(){
        browser
        //checking menu in the header
        //subscribe bar 
        .useXpath()
        .pause(4000)
        //menu on top
        .waitForElementPresent('//*[@id="comp-kzzs3ed8itemsContainer"]', 3000)
        .verify.visible('//*[@id="comp-kzzs3ed8itemsContainer"]')
        .verify.ok(true,"menu is appearing")
        //check logo
        .waitForElementPresent('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[1]/div[2]/div[2]/div[2]/div/div/p/span/span/a',3000)
        .verify.visible("/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[1]/div[2]/div[2]/div[2]/div/div/p/span/span/a")
        .verify.ok(true,"Check logo")

        //check every product under this table
        browser
        .useXpath()
        .waitForElementPresent('//*[@id="comp-kzzs3ed8itemsContainer"]', 4000)
        .useCss()
        var menuOption = '#comp-kzzs3ed8x > a:nth-child(1)'
        var valueX = 0
        var screenshot = 0
        for (var i = 1; i < 6; i++) {
            valueX = valueX+1;
            screenshot = screenshot+1
            let nextLink = menuOption.replace("x", valueX);
            console.log("item ->  " + nextLink)
            //hover over the menu item
            browser
            .useCss()
            .pause(4000)
            .waitForElementPresent(nextLink, 2000)  
            .verify.visible(nextLink)  
            .pause(3000)
            .saveScreenshot('pathForScreenshots/Production/Screenshots/OptionMenu#' + screenshot +'.png')
            .pause(3000)
            //check url status code
            browser
            .useCss()
            .getAttribute(nextLink, "href",function(result){
                let pageUrl = result.value
                console.log("the page is " +pageUrl)
                browser
                .verify.ok(true,"The page is " +pageUrl)
                const requestPage = require('request');
                requestPage(pageUrl, function (error, response, body) {
                        let responseP = response.statusCode;
                        console.log("RESPONSE STATUS URL: " + responseP)
                        if (responseP === 404){
                            browser
                            .verify.equal(response.statusCode, 200)
                            .verify.ok(false,"this link  does not work: "+pageUrl)
                        }
                        else {
                            browser
                            .verify.ok(true,"Page status code OK")

                        }
                    })

                })
            .click(nextLink)
        }
    }

return this;
}//end big module

