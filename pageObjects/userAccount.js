//This example is going to test: login without an account, an existed account trying to register, creating an account and login with a valid user
//Still pending other screnarios like enter invalid data(data type), data/values format for each field, submit without data.
module.exports = function (browser){
    this.openBrowser = function() {
        browser
            .url('https://riveravargasmiguel.wixsite.com/mollystorm')
            .resizeWindow(1400, 3000)
        return browser
    };//end openBrowser

    this.page1 = function(){
        browser
            .verify.ok(true, "'https://riveravargasmiguel.wixsite.com/mollystorm' opened sucessfully")
            .useXpath()
            .waitForElementVisible('/html/body', 10000)
            .pause(5000)
            // Take one screenshot 
            .saveScreenshot('pathtosavescreenchots/Screenshots/home_Page.png')
            .pause(3000)
            const request = require('request');
            request('https://riveravargasmiguel.wixsite.com/mollystorm', function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Beliebtester Shop: " + responseURL)
                    if (request === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"URL does not work")
                    }
                    else {
                        browser
                        .verify.ok(true,"Url is good")
                    }
                })
    }
//general action to use to interact with the page


    this.switching = function(){
        browser
        //Switch back to the first window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[0];
            this.switchWindow(newWindow);
        })
    }

    this.switchingWin2 = function(){
        browser
        //Switch back to the second window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[1];
            this.switchWindow(newWindow);
        })
    }


    this.back = function() {
        browser
        .execute(function () {
            window.history.back()
        })//to go to the previous page
        .pause(4000)
    }

    this.previews = function(){
        browser
        .back()
        consentMessage()
        browser
        .useXpath()
    }

    //This is a valid user account for test
    const userEmail = 'TestName@test4u.com'
    const userPassword =  'Testing1234!'


    this.closeLoginSignup= function(){
        browser
        //closing login page
        .useXpath()
        .waitForElementVisible('/html/body/div[1]/div/div[6]/div[2]/button')
        .verify.visible('/html/body/div[1]/div/div[6]/div[2]/button')
        .click('/html/body/div[1]/div/div[6]/div[2]/button')
        .verify.ok(true, "closing login page") 
    }


    //important parts of the sign up/log in  page


    this.signUpSection = function(){
        browser
        //go to sign up
        .useXpath()
        .pause(4000)
        .waitForElementPresent("/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/button/span", 2000)
        .verify.visible("/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/button/span")
        .click('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/button/span')
        .verify.ok(true,"Access to user page to login'")
        .pause(2000)
        .saveScreenshot('pathtosavescreenchots/Screenshots/user.png')
        .pause(2000)
    }

    //to logout
    this.logout = function(){
        browser
        .useXpath()
        .waitForElementVisible('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/button/div[2]')
        .verify.visible('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/button/div[2]')
        .click('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/button/div[2]')
        .verify.ok(true, "Open user drop down") 
        .pause(3000)
        //logout
        .waitForElementPresent('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/div/nav/ul/li[7]/div/span', 4000)
        .verify.visible('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/div/nav/ul/li[7]/div/span')
        .click('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/div/nav/ul/li[7]/div/span')
        .pause(2000)
        .saveScreenshot('pathtosavescreenchots/Screenshots/logout.png')
        .pause(5000)
    }

    //to check if there is someone loguedIn
    this.loguedIn = function(){
        browser
        .useXpath()
        .waitForElementVisible('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/button/div[2]')
        .verify.visible('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/button/div[2]')
        .click('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/button/div[2]')
        .verify.ok(true, "Open user drop down") 
        .pause(3000)
        //logout
        .waitForElementPresent('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/div/nav/ul/li[7]/div/span', 4000)
        .verify.visible('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/div/nav/ul/li[7]/div/span')
        .pause(2000)
        .saveScreenshot('pathtosavescreenchots/Screenshots/loguedIn.png')
        .pause(5000)
        //close dropdown
        .click('/html/body/div[1]/div/div[4]/div/header/div/div[2]/div[2]/div/div/section[2]/div[2]/div[3]/div[2]/div/div[1]/div/button/div[2]')
    }

    this.loginSection = function(){
        browser
        .useXpath()
        //go to log in
        .waitForElementVisible('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/div/button')
        .verify.visible('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/div/button')
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/div/button','Log In')
        .click('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/div/button')
        .verify.ok(true, "Go to Log in") 
    }


    //log in without user account
    this.uLogNoAccount = function(){
        signUpSection()
        loginSection()
        //go to log in
        browser
        .useXpath()
        //email label should appear
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[1]/div[1]/div/div/label", 2000)
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[1]/div[1]/div/div/label','Email')
        .verify.ok(true,"email label should appear")
        //enter an email in the email field
        .waitForElementPresent('//*[@id="input_input_emailInput_SM_ROOT_COMP14"]', 2000)
        .click('//*[@id="input_input_emailInput_SM_ROOT_COMP14"]')
        .setValue('//*[@id="input_input_emailInput_SM_ROOT_COMP14"]','test@testx.com')
        .verify.ok(true,"Enter an email address")
        //password label should appear
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[1]/div[2]/div/div/label", 2000)
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[1]/div[2]/div/div/label','Password')
        //Enter a password
        .waitForElementPresent('//*[@id="input_input_passwordInput_SM_ROOT_COMP14"]', 2000)
        .click('//*[@id="input_input_passwordInput_SM_ROOT_COMP14"]')
        .setValue('//*[@id="input_input_passwordInput_SM_ROOT_COMP14"]','testingx123!')
        //click on Log in
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[3]/div/button", 2000)
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[3]/div/button','Log In')
        .click('/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[3]/div/button')
        //should appear error message
        .waitForElementPresent('//*[@id="siteMembersInputErrorMessage_passwordInput_SM_ROOT_COMP14"]', 2000)
        .verify.containsText('//*[@id="siteMembersInputErrorMessage_passwordInput_SM_ROOT_COMP14"]','Wrong email or password')
        .verify.ok(true,"Enter an invalid user and password- user does not have an acccount test")
        .pause(2000)
        .saveScreenshot('pathtosavescreenchots/Screenshots/userNoAccount.png')
        .pause(2000)
        closeLoginSignup()
        
    }

    //create an account, but the account already exist
    this.alreadyAccount =function(){
        browser
        .url('https://riveravargasmiguel.wixsite.com/mollystorm')
        signUpSection()
        browser
        .useXpath()
        .pause(2000)
        //go to Create Account
        //check that email label is present
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[1]/div[1]/div/div/label", 2000)
        .verify.visible("/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[1]/div[1]/div/div/label")
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[1]/div[1]/div/div/label','Email')
        //enter a email
        .waitForElementPresent('//*[@id="input_input_emailInput_SM_ROOT_COMP13"]', 4000)
        .verify.visible('//*[@id="input_input_emailInput_SM_ROOT_COMP13"]')
        .click('//*[@id="input_input_emailInput_SM_ROOT_COMP13"]')
        .setValue('//*[@id="input_input_emailInput_SM_ROOT_COMP13"]',userEmail)
        .verify.ok(true,"Enter an email")


        //check that password label is present
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[1]/div[2]/div/div/label", 2000)
        .verify.visible("/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[1]/div[2]/div/div/label")
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[1]/div[2]/div/div/label','Password')
        //enter a password
        .waitForElementPresent('//*[@id="input_input_passwordInput_SM_ROOT_COMP13"]', 4000)
        .verify.visible('//*[@id="input_input_passwordInput_SM_ROOT_COMP13"]')
        .click('//*[@id="input_input_passwordInput_SM_ROOT_COMP13"]')
        .setValue('//*[@id="input_input_passwordInput_SM_ROOT_COMP13"]',userPassword)
        .verify.ok(true,"Enter a password")


        //click on creat account
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[2]/div/button", 2000)
        .verify.visible("/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[2]/div/button")
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[2]/div/button','Sign Up')
        .click('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[2]/div/button')
        .pause(5000)

        //verify the validation message
        // .waitForElementPresent('//*[@id="siteMembersInputErrorMessage_emailInput_SM_ROOT_COMP34"]', 2000)
        // .verify.visible('//*[@id="siteMembersInputErrorMessage_emailInput_SM_ROOT_COMP34"]')
        // .verify.containsText('//*[@id="siteMembersInputErrorMessage_emailInput_SM_ROOT_COMP34"]','An account with this email already exists.')
        .pause(2000)
        .saveScreenshot('pathtosavescreenchots/Screenshots/AlreadyanUser.png')
        .pause(2000)
        closeLoginSignup()

    }

    //create a new account and verify it was created
    this.creatAccount =function(){
        browser
        .url('https://riveravargasmiguel.wixsite.com/mollystorm')
        signUpSection()
        browser
        .useXpath()
        .pause(2000)
        //go to Create Account    
        //enter a email
        //GENERATE AN EMAIL ADDRESS
        var nameEmail = function () {
            // Math.random should be unique because of its seeding algorithm.
            // Convert it to base 36 (numbers + letters), and grab the first 9 characters
            // after the decimal.
            return Math.random().toString(36).substr(2, 9);
          };
        let emailUser = nameEmail()+'@domain.com'
        browser
        .verify.ok(true,"Generating new email: "+ emailUser)
        //enter an email
        browser
        .useXpath()
        .waitForElementPresent('//*[@id="input_input_emailInput_SM_ROOT_COMP13"]', 4000)
        .verify.visible('//*[@id="input_input_emailInput_SM_ROOT_COMP13"]')
        .click('//*[@id="input_input_emailInput_SM_ROOT_COMP13"]')
        .setValue('//*[@id="input_input_emailInput_SM_ROOT_COMP13"]',emailUser)
        .verify.ok(true,"Enter an email")

        

        //GENERATE A PASSWORD
        var passw = function () {
            // Math.random should be unique because of its seeding algorithm.
            // Convert it to base 36 (numbers + letters), and grab the first 9 characters
            // after the decimal.
            return Math.random().toString(36).substr(2, 9)+ "!";
          };
        let pass = passw()
        browser
        .verify.ok(true,"Generating new passowrd: "+ pass)
   
        //enter a password
        browser
        .useXpath()
        //enter a password
        .waitForElementPresent('//*[@id="input_input_passwordInput_SM_ROOT_COMP13"]', 4000)
        .verify.visible('//*[@id="input_input_passwordInput_SM_ROOT_COMP13"]')
        .click('//*[@id="input_input_passwordInput_SM_ROOT_COMP13"]')
        .setValue('//*[@id="input_input_passwordInput_SM_ROOT_COMP13"]',pass)
        .verify.ok(true,"Enter a password")
        .pause(2000)
        .saveScreenshot('pathtosavescreenchots/Screenshots/EnterNewUser.png')
        .pause(2000)

        //click on creat account
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[2]/div/button", 2000)
        .verify.visible("/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[2]/div/button")
        .pause(2000)
        .click('/html/body/div[1]/div/div[6]/div[2]/div[1]/div[1]/form/div[2]/div/button')
        .pause(2000)
        .saveScreenshot('pathtosavescreenchots/Screenshots/NewUserMessage.png')
        .pause(2000)

        //verify the new account is active
        //click on the user icon
        loguedIn()
        browser
        .pause(2000)
        logout()

    }

    //log in with a valid user account
    this.loginValidUser = function(){
        browser
        .url('https://riveravargasmiguel.wixsite.com/mollystorm')
        signUpSection()
        loginSection()
        browser
        .useXpath()

        //email label should appear
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[1]/div[1]/div/div/label", 2000)
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[1]/div[1]/div/div/label','Email')
        .verify.ok(true,"email label should appear")
        //enter an email in the email field
        .waitForElementPresent('//*[@id="input_input_emailInput_SM_ROOT_COMP14"]', 2000)
        .click('//*[@id="input_input_emailInput_SM_ROOT_COMP14"]')
        .setValue('//*[@id="input_input_emailInput_SM_ROOT_COMP14"]',userEmail)
        .verify.ok(true,"Enter an email address")
        //password label should appear
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[1]/div[2]/div/div/label", 2000)
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[1]/div[2]/div/div/label','Password')
        //Enter a password
        .waitForElementPresent('//*[@id="input_input_passwordInput_SM_ROOT_COMP14"]', 2000)
        .click('//*[@id="input_input_passwordInput_SM_ROOT_COMP14"]')
        .setValue('//*[@id="input_input_passwordInput_SM_ROOT_COMP14"]',userPassword)
        //click on Sign in
        .waitForElementPresent("/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[3]/div/button", 2000)
        .verify.containsText('/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[3]/div/button','Log In')
        .click('/html/body/div[1]/div/div[6]/div[2]/div[1]/div/form/div[3]/div/button')
        .pause(2000)
        .saveScreenshot('pathtosavescreenchots/Screenshots/loginValidUser.png')
        .pause(5000)
        logout()
        }





   




  





return this;
}//end big module

