var general = require('../pageObjects/userAccount');


module.exports = {
    "@tags" : ['user'],
    before: function (browser) {
        general(browser).openBrowser();
    },
    'TEST SUITE: User Sign Up and LogIn - test cases' : function(browser){
        general(browser).page1();
        
    },
    '1. Check: login without an account': function (browser){
        general(browser).uLogNoAccount();
    },
    '2. Check: an existed account trying to register': function (browser){
        general(browser).alreadyAccount();
    },
    '3. Check: creating an account': function (browser){
       general(browser).creatAccount();
    },
   '4. Check: login with a valid user': function (browser){
       general(browser).loginValidUser();
    },
   


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module