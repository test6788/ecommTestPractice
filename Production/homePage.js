var general = require('../pageObjects/general');


module.exports = {
    "@tags" : ['general'],
    before: function (browser) {
        general(browser).openBrowser();
    },
    'TEST SUITE: Home page' : function(browser){
        general(browser).page1();
        
    },
    '1. Check home page general features-Header Menu': function (browser){
        general(browser).headMenu();
    },
   


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

